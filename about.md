---
layout: page
title: About
permalink: /about/
---

This blog is written as a tracker for my work around ZK proofs and implementing
them in order to create a protocol around off chain smart contracts
